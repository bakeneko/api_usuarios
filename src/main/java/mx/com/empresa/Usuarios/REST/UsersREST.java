package mx.com.empresa.Usuarios.REST;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import mx.com.empresa.Usuarios.DAO.UsuarioDAO;
import mx.com.empresa.Usuarios.entitys.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
@RestController
@RequestMapping("users")
public class UsersREST {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @GetMapping(value = "hello")
//    @RequestMapping(value="hello", method = RequestMethod.GET)
    public ResponseEntity<String> hello() {
        String hola = "Hola mundo";
        return ResponseEntity.ok(hola);
    }

    @GetMapping
    public ResponseEntity<List<Usuario>> getUsuarios() {
        List<Usuario> listUsers = usuarioDAO.findAll();
        return ResponseEntity.ok(listUsers);
    }

    @RequestMapping(value = "{idUser}")
    public ResponseEntity<Usuario> getUsuariosById(@PathVariable("idUser") int idUser) {
        Optional<Usuario> user = usuarioDAO.findById(idUser);
        if (!user.isPresent()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(user.get());
    }

    @PostMapping
    public ResponseEntity<Usuario> createUsuario(Usuario user) {
        Usuario newUser = usuarioDAO.save(user);
        return ResponseEntity.ok(newUser);
    }

}
