package mx.com.empresa.Usuarios.DAO;

import mx.com.empresa.Usuarios.entitys.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
public interface UsuarioDAO extends JpaRepository<Usuario, Integer>{
    
}
